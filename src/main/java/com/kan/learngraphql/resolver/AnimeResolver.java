package com.kan.learngraphql.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kan.learngraphql.model.Anime;
import com.kan.learngraphql.model.Author;
import com.kan.learngraphql.repository.AuthorRepository;

import graphql.kickstart.tools.GraphQLResolver;

@Component
public class AnimeResolver implements GraphQLResolver<Anime> {

	@Autowired
	private AuthorRepository authorRepository;
	
	public AnimeResolver(AuthorRepository authorRepository) {
		this.authorRepository = authorRepository;
	}
	
	public Author getAuthor(Anime anime) {
		return authorRepository.findById(anime.getAuthor().getId()).orElseThrow(null);
	}
	
}
