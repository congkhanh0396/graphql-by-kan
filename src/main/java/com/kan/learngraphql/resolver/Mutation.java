package com.kan.learngraphql.resolver;

import java.util.Optional;

import com.kan.learngraphql.publisher.ChatPublisher;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.kan.learngraphql.model.Anime;
import com.kan.learngraphql.model.Author;
import com.kan.learngraphql.model.Chat;
import com.kan.learngraphql.repository.AnimeRepository;
import com.kan.learngraphql.repository.AuthorRepository;
import com.kan.learngraphql.repository.ChatRepository;

import graphql.kickstart.tools.GraphQLMutationResolver;
import javassist.NotFoundException;

@AllArgsConstructor
@Component
public class Mutation implements GraphQLMutationResolver {
	private final AuthorRepository authorRepository;
	private final AnimeRepository animeRepository;
	private final ChatRepository chatReposiroty;

	// 保存された Chatごとに Subscriber(今回では GraphQL Subscription)に 送信してくれる人 に通知する人
	private final ChatPublisher chatEmitToPublisher;

//	@Autowired
//	public Mutation(AuthorRepository authorRepository, AnimeRepository animeRepository, ChatRepository chatReposiroty) {
//		this.authorRepository = authorRepository;
//		this.animeRepository = animeRepository;
//		this.chatReposiroty = chatReposiroty;
//	}

	public Author createAuthor(String name, String age) {
		Author author = new Author();
		author.setName(name);
		author.setAge(age);

		authorRepository.save(author);

		return author;
	}

	public Boolean deleteAuthor(Long id) {
		authorRepository.deleteById(id);
		return true;
	}

	public Author updateAuthor(Long id, String name, String age) throws NotFoundException {
		Optional<Author> optAuthor = authorRepository.findById(id);
		if (optAuthor.isPresent()) {
			Author author = optAuthor.get();
			if (name != null)
				author.setName(name);
			if (age != null)
				author.setAge(age);
			authorRepository.save(author);
			return author;
		}
		throw new NotFoundException("Not found Author to update!");
	}

	public Anime createAnime(String title, String description, String opSong, String edSong, Long authorID) {
		Anime animation = new Anime();
		animation.setAuthor(new Author(authorID));
		animation.setTitle(title);
		animation.setDescription(description);
		animation.setOpSong(opSong);
		animation.setEdSong(edSong);
		animeRepository.save(animation);
		return animation;
	}
	
	public Boolean deleteAnime(Long id) {
		animeRepository.deleteById(id);
		return true;
	}
	
	
	public Chat sendMessage(String name, String message ) {
		Chat chat = new Chat(); 
		chat.setName(name);
		chat.setMessage(message);
		chatReposiroty.save(chat);

		// 保存された Chatを GraphQL Subscriptionに通知する人に 通知
		chatEmitToPublisher.emit(chat);

		return chat;
	}

}
