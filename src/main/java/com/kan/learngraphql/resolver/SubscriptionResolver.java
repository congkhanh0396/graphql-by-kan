package com.kan.learngraphql.resolver;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.kan.learngraphql.publisher.ChatPublisher;
import com.kan.learngraphql.publisher.ChatPublisherImpl;
import lombok.AllArgsConstructor;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kan.learngraphql.model.Chat;
import com.kan.learngraphql.repository.ChatRepository;

import graphql.kickstart.tools.GraphQLSubscriptionResolver;

@AllArgsConstructor
@Component
public class SubscriptionResolver implements GraphQLSubscriptionResolver {

	private final ChatRepository chatRepository;

	// 保存された Chatごとに Subscriber(今回では GraphQL Subscription)に 送信してくれる人
	private final ChatPublisher chatPublisher;

//	@Autowired
//	public SubscriptionResolver(ChatRepository chatRepository) {
//		this.chatRepository = chatRepository;
//	}

	public Publisher<Chat> messageSentByName(String name) { return chatPublisher.publishByName(name); }

	/**
	 * GraphQL Subscription for ChatBox として... 保存された Chatごとに Subscriptionで 通知する仕組み
	 * @return 保存された Chatを Subscriber(今回では GraphQL Subscription)に 送信してくれる人
	 */
	public Publisher<Chat> messageSentAnyone() { return chatPublisher.publish(); }

	public Publisher<Iterable<Chat>> messageSent(){
		// これだと 1秒ごとに 全てのChatデータを通知する Subscriptionなので、ChatBoxとしては 間違ってはいないですが
		// Chatの量が多ければ多いほど大変辛い Subscriptionになってしまうので
		// Mutationで登録された 新しいもの 1件ずつ Subscription に Publish(通知する) Publisherにしてみましょう！
		return new Publisher<Iterable<Chat>>() {
			@Override
			public void subscribe(Subscriber<? super Iterable<Chat>> s) {
				Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() -> {
					List<Chat> chats = chatRepository.findAll();
					s.onNext(chats);
				}, 0, 1, TimeUnit.SECONDS);
			}

		};

}}
