package com.kan.learngraphql.resolver;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.kan.learngraphql.model.Anime;
import com.kan.learngraphql.model.Author;
import com.kan.learngraphql.model.Chat;
import com.kan.learngraphql.repository.AnimeRepository;
import com.kan.learngraphql.repository.AuthorRepository;
import com.kan.learngraphql.repository.ChatRepository;

import graphql.kickstart.tools.GraphQLQueryResolver;

@Component
public class QueryResolver implements GraphQLQueryResolver {
	private AnimeRepository animeRepository;
	private AuthorRepository authorRepository;
	private ChatRepository chatRepository;
	
	@Autowired
	public QueryResolver(AuthorRepository authorRepository, AnimeRepository animeRepository, ChatRepository chatRepository) {
		this.authorRepository = authorRepository;
		this.animeRepository = animeRepository;
		this.chatRepository = chatRepository;

	}

	public Optional<Author> author(Long id) {
		return authorRepository.findById(id);
	}
	
	public Optional<Anime> anime(Long id) {
		return animeRepository.findById(id);
	}
	
	
	public Iterable<Author> findAllAuthors() {
		return authorRepository.findAll();
	}
	
	public Iterable<Anime> findAllAnimes() {
		return animeRepository.findAll();
	}
	
	public Iterable<Chat> findAllMessage() {
		return chatRepository.findAll();
	}
	
	
}
