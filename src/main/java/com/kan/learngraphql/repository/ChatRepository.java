package com.kan.learngraphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kan.learngraphql.model.Chat;

public interface ChatRepository extends JpaRepository<Chat, Long> {

}
