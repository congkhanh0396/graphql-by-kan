package com.kan.learngraphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kan.learngraphql.model.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {

}
