package com.kan.learngraphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kan.learngraphql.model.Anime;

public interface AnimeRepository extends JpaRepository<Anime, Long> {

}
