package com.kan.learngraphql.publisher;

import org.reactivestreams.Publisher;

/**
 * 独自  emitと Publisherを定義するProcessor on reactive-streams
 * @param <T> emitとPublisherするときの方
 */
public interface MyPublisher<T> {
	/**
	 * Publisher of an event emitted.
	 */
	Publisher<T> publish();
	/**
	 * Emit an event.
	 */
	void emit(T t);
}
