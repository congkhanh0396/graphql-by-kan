package com.kan.learngraphql.publisher;

import com.kan.learngraphql.model.Chat;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;

/**
 * Chat Publisher実装 on reactor core
 */
@Component
public class ChatPublisherImpl implements ChatPublisher {
	/** (mono) Emitter is emit Chat to Publisher (保存された Chatを 下のPublisher へ 送信してくれる人) */
	private ObservableEmitter<Chat> emitter;
	/** (mono) Publisher is emit the changed Chat to GraphQL Subscription (生産者クラス: 保存された Chatを Subscriber(今回では GraphQL Subscription)に 送信してくれる人) */
	private final Flowable<Chat> publisher;

	public ChatPublisherImpl() {
		// 目的 : メッセージ送信されるたびに (mutation sendMessage) 登録または更新された Chatを Subscription に通知するための 非同期的な リアクティブプログラミング(Reactive Programing)
		// any one Mutation Chat sendMessage(...) -> chatRepository.save ( -> emit the change Chat to Publisher ) -> return Chat
		//                                                                       ↓ any time
		// any one Subscription Publisher<Chat> messageSent(...) -> return = send the changed Chat to Subscriber(GraphQL Subscription)
		//                                                                   Publisher is will send the changed chat

		// 何回でも新しい Chat(mono) は発生して良いので Observableにて Publisherに通知してくれる Emitterを定義
		Observable<Chat> commentUpdateObservable = Observable.create(emitter -> { this.emitter = emitter; });

		// ユーザーは誰でもいい作りになっているので 誰かが sendMessage で登録更新した Chatを Publisherへ通知する環境定義
		ConnectableObservable<Chat> connectableObservable = commentUpdateObservable.share().publish();
		// Chat(データ) 通知のする環境開始
		connectableObservable.connect();

		// Emitter to Publisherにて Chat を通知したら Publishするようにしたい かつ 何回でも動いていいので BackpressureStrategy.BUFFER で定義
		this.publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
	}

	/**
	 * for Chat GraphQL Subscription(Subscriber)
	 * Publish an event that a chat is registered.
	 */
	public Publisher<Chat> publish() { return publisher; }

	/**
	 * Emit an event to publisher.
	 * @param chat a chat
	 */
	public void emit(Chat chat) { this.emitter.onNext(chat); }

	/**
	 * for Chat GraphQL Subscription(Subscriber) filter by name
	 * Publish an event that a chat of specific user is registered.
	 *
	 * @param name 特定のユーザーを名前で限定
	 */
	@Override
	public Publisher<Chat> publishByName(String name) {
		return publisher.filter(chat -> name.equals(chat.getName()));
	}
}
