package com.kan.learngraphql.publisher;

import com.kan.learngraphql.model.Chat;
import org.reactivestreams.Publisher;

public interface ChatPublisher extends MyPublisher<Chat> {
	/**
	 * for Chat GraphQL Subscription(Subscriber) filter by name
	 * Publish an event that a chat of specific user is registered.
	 */
	Publisher<Chat> publishByName(String name);
}
